/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import view.FrameDataNilaiSiswa;
import java.sql.SQLException;

/**
 *
 * @author Dz
 */
public interface controllerDataNilaiSiswa {
    public void insert(FrameDataNilaiSiswa frm) throws SQLException;
    public void edit(FrameDataNilaiSiswa frm) throws SQLException;
    public void update(FrameDataNilaiSiswa frm) throws SQLException;
    public void delete(FrameDataNilaiSiswa frm) throws SQLException;
    public void refresh(FrameDataNilaiSiswa frm);
    public void exit(FrameDataNilaiSiswa frm);
    public void tampilDataSiswa(FrameDataNilaiSiswa frm) throws SQLException;
    public void tampiDataNilai(FrameDataNilaiSiswa frm) throws SQLException;
    public void getPilihPelajaran(FrameDataNilaiSiswa frm) throws SQLException;
    
    public void tabelSiswaGetClicked(FrameDataNilaiSiswa frm) throws SQLException;
    
    public void filterKelas(FrameDataNilaiSiswa frm) throws SQLException;
    public void filterJurusan(FrameDataNilaiSiswa frm) throws SQLException;
}
