package koneksi;

import com.mysql.jdbc.Connection;
//import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class koneksi {
    private static Connection conn;    
    public Statement stt;
    
    public static Connection getKoneksi() {
        String URL = "jdbc:mysql://127.0.0.1/siswaakademik"; 
        String USR = "root";
        String PASS = "";
        try {
            conn = (Connection) DriverManager.getConnection(URL,USR,PASS);
        } catch (SQLException ex) {
            Logger.getLogger(koneksi.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            System.out.print("ERR KONEKSI");
        }
        return conn;
    }
    
    public Statement state() { //belajar oop
        getKoneksi();
        try {
            stt = getKoneksi().createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(koneksi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stt;
    }
}
