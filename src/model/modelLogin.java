/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import view.FrameDebitSiswa;
import view.FrameDataNilaiSiswa;
import view.FrameLogin;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Dz
 */
public class modelLogin implements controller.controllerLogin {
private final Connection conn = koneksi.koneksi.getKoneksi();
//    public FrameLogin frm;

    @Override
    public void btnlogin(FrameLogin frmLogin) throws SQLException {
        Status(frmLogin);
    }

    @Override
    public void Status(FrameLogin frmLogin) throws SQLException {
        if(frmLogin.cbb_pil.getSelectedItem().equals(FrameLogin.pil[0])) {
            JOptionPane.showMessageDialog(null, FrameLogin.pil[0]);
        } else if(frmLogin.cbb_pil.getSelectedItem().equals(FrameLogin.pil[1])) {
//            frmLogin.txt_username.setText(pil[1]);
            String query = "SELECT username,password FROM usr_login WHERE "
                    + "status='" + frmLogin.cbb_pil.getSelectedItem().toString().toLowerCase() + "' AND " //combobox sebagai pembeda antara dua kategori
                    + "username='" + frmLogin.txt_username.getText() + "' AND "
                    + "password='" + frmLogin.txt_password.getText() + "';";
            try {
                if(frmLogin.txt_username.getText().equals("") || frmLogin.txt_password.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Periksa Inputan anda !");
                } else {
                    Statement stt = conn.createStatement();
                    ResultSet res = stt.executeQuery(query);
                    while(res.next()){
                        res.getString("username");
                        res.getString("password");                        
                        
                        System.out.println(frmLogin.cbb_pil.getSelectedItem().toString().toLowerCase());
//                        System.out.println("Berhasil Login Menggunakan Username "+res.getString("username"));
//                        System.out.println("Berhasil Login Menggunakan Password "+res.getString("password"));                        
                        FrameDebitSiswa frmAkdm = new FrameDebitSiswa();
                        frmAkdm.setVisible(true);
                        frmLogin.dispose();                                          
                    }
                stt.close();
                res.close();                
                }
            } catch (SQLException ex) {
                Logger.getLogger(FrameLogin.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            } catch (HeadlessException e) {
                Logger.getLogger(FrameLogin.class.getName()).log((Level.ALL), e.getMessage());
                System.out.println("Err At logger 2");
            }
        } else if(frmLogin.cbb_pil.getSelectedItem().equals(frmLogin.pil[2])) {
            String query1 = "SELECT username,password FROM usr_login WHERE "
                    + "status='" + frmLogin.cbb_pil.getSelectedItem().toString().toLowerCase() + "' AND " //combobox sebagai pembeda antara dua kategori
                    + "username='" + frmLogin.txt_username.getText() + "' AND "
                    + "password='" + frmLogin.txt_password.getText() + "';";
            try {
                if(frmLogin.txt_username.getText().equals("") || frmLogin.txt_password.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Periksa Inputan Anda !");
                } else {
                    Statement stt = conn.createStatement();
                    ResultSet res = stt.executeQuery(query1);

                    while(res.next()){
                        res.getString("username");
                        res.getString("password");

                        FrameDataNilaiSiswa frmguru = new FrameDataNilaiSiswa();
                        frmguru.setVisible(true);
                        frmLogin.dispose();
                    }
                stt.close();
                res.close();
                }                
            } catch (SQLException ex) {
                Logger.getLogger(FrameLogin.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                System.out.println("ERR guru ?");
            } catch (HeadlessException e) {
                Logger.getLogger(FrameLogin.class.getName()).log((Level.ALL), e.getMessage());
                System.out.println("ERR Guru ??");
            }
        }
    }

    @Override
    public void cancel(FrameLogin frmLogin) {
        frmLogin.dispose();
    }    
}
