/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import koneksi.koneksi;
import view.FrameDataNilaiSiswa;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import controller.controllerDataNilaiSiswa;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author Dz
 */
public class modelDataNilaiSiswa implements controllerDataNilaiSiswa {
    public DefaultTableModel mDataSiswa,mDataNilai;
    public static Connection conn = koneksi.getKoneksi();
    
    private final String[] kelas = {"Filter By Kelas","KELAS-1","KELAS-2","KELAS-3"};
    private final String[] jurusan = {"Filter By Jurusan","TKJ","MULTIMEDIA","ELEKTRO","OTOMOTIF","MESIN"};
    
    private String tempRow;
    
    
    public void getModelTabelSiswa(FrameDataNilaiSiswa frm) {
        mDataSiswa = new DefaultTableModel();
        frm.tbl_dataSiswa.setModel(mDataSiswa);
        
        mDataSiswa.addColumn("NIS");
        mDataSiswa.addColumn("NAMA");
        mDataSiswa.addColumn("KELAS");
        mDataSiswa.addColumn("JENIS KELAMIN");
        mDataSiswa.addColumn("TAHUN AJARAN");
        frm.tbl_dataSiswa.setDefaultEditor(Object.class, null);
    }
    
    public void getModelTabelNilai(FrameDataNilaiSiswa frm) {
        mDataNilai = new DefaultTableModel();
        frm.tbl_nilaiSiswa.setModel(mDataNilai);
        
        mDataNilai.addColumn("NIS");
        mDataNilai.addColumn("NAMA");
        mDataNilai.addColumn("KELAS");
        mDataNilai.addColumn("PELAJARAN");
        mDataNilai.addColumn("ABSEN");
        mDataNilai.addColumn("TUGAS");
        mDataNilai.addColumn("UTS");
        mDataNilai.addColumn("UAS");
        mDataNilai.addColumn("TOTAL");
        mDataNilai.addColumn("INDEX");
        frm.tbl_nilaiSiswa.setDefaultEditor(Object.class, null);
    }
    
    public void getFilterKelas(FrameDataNilaiSiswa frm) {
        frm.cbb_angkatan.removeAllItems();
        frm.cbb_angkatan.addItem(kelas[0]);
        frm.cbb_angkatan.addItem(kelas[1]);
        frm.cbb_angkatan.addItem(kelas[2]);
        frm.cbb_angkatan.addItem(kelas[3]);
    }
    
    public void getFilterJurusan(FrameDataNilaiSiswa frm) {
        frm.cbb_jurusan.removeAllItems();
        frm.cbb_jurusan.addItem(jurusan[0]);
        frm.cbb_jurusan.addItem(jurusan[1]);
        frm.cbb_jurusan.addItem(jurusan[2]);
        frm.cbb_jurusan.addItem(jurusan[3]);
        frm.cbb_jurusan.addItem(jurusan[4]);
        frm.cbb_jurusan.addItem(jurusan[5]);
    }
        
    @Override
    public void insert(FrameDataNilaiSiswa frm) throws SQLException {
        
    }

    @Override
    public void edit(FrameDataNilaiSiswa frm) throws SQLException {
        
    }

    @Override
    public void update(FrameDataNilaiSiswa frm) throws SQLException {
        
    }

    @Override
    public void delete(FrameDataNilaiSiswa frm) throws SQLException {
        
    }

    @Override
    public void refresh(FrameDataNilaiSiswa frm) {
        frm.txt_nis.setText("");
        frm.txt_nama.setText("");
        frm.txt_kelas.setText("");
        frm.txt_angkatan.setText("");
        frm.txt_jenisKelamin.setText("");
        frm.txt_nisNilai.setText("");
        frm.txt_filterJurusan.setText("");
        frm.txt_filterKelas.setText("");
        getFilterJurusan(frm);
        getFilterKelas(frm);
        try {
            tampiDataNilai(frm);
            tampilDataSiswa(frm);
            getPilihPelajaran(frm);
        } catch (SQLException ex) {
            Logger.getLogger(modelDataNilaiSiswa.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
        JOptionPane.showMessageDialog(frm, "Data Telah Di Refresh");
    }

    @Override
    public void exit(FrameDataNilaiSiswa frm) {
        try {
            conn.close();
            frm.dispose();
        } catch (SQLException ex) {
            Logger.getLogger(modelDataNilaiSiswa.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void filterKelas(FrameDataNilaiSiswa frm) throws SQLException {
        if(frm.cbb_angkatan.getSelectedItem() == kelas[1]) {
            frm.txt_filterKelas.setText(kelas[1]);
            getModelTabelSiswa(frm);
            try {
                mDataSiswa.getDataVector().removeAllElements();
                mDataSiswa.fireTableDataChanged();

                String query = "SELECT * FROM datasiswakelas1";
                Statement stt = conn.createStatement();
                ResultSet res = stt.executeQuery(query);
                while(res.next()) {
                    Object[] obj = new Object[5];
                        obj[0] = res.getString("nis");
                        obj[1] = res.getString("nama");
                        obj[2] = res.getString("nama_kelas");
                        obj[3] = res.getString("jenis_kelamin");
                        obj[4] = res.getString("tahun_ajaran");
                        mDataSiswa.addRow(obj);
                }
                stt.close();
                res.close();
            } catch (SQLException e){
                e.printStackTrace();
                System.out.println(e.getSQLState());
            }
        } else if(frm.cbb_angkatan.getSelectedItem() == kelas[2]) {
            frm.txt_filterKelas.setText(kelas[2]);
            getModelTabelSiswa(frm);
            try {
                mDataSiswa.getDataVector().removeAllElements();
                mDataSiswa.fireTableDataChanged();

                String query = "SELECT * FROM datasiswakelas2";
                Statement stt = conn.createStatement();
                ResultSet res = stt.executeQuery(query);
                while(res.next()) {
                    Object[] obj = new Object[5];
                        obj[0] = res.getString("nis");
                        obj[1] = res.getString("nama");
                        obj[2] = res.getString("nama_kelas");
                        obj[3] = res.getString("jenis_kelamin");
                        obj[4] = res.getString("tahun_ajaran");
                        mDataSiswa.addRow(obj);
                }
                stt.close();
                res.close();
            } catch (SQLException e){
                e.printStackTrace();
                System.out.println(e.getSQLState());
            }
        } else if(frm.cbb_angkatan.getSelectedItem() == kelas[3]) {
            frm.txt_filterKelas.setText(kelas[3]);
            getModelTabelSiswa(frm);
            try {    
                mDataSiswa.getDataVector().removeAllElements();
                mDataSiswa.fireTableDataChanged();

                String query = "SELECT * FROM datasiswakelas3";
                Statement stt = conn.createStatement();
                ResultSet res = stt.executeQuery(query);
                while(res.next()) {
                    Object[] obj = new Object[5];
                        obj[0] = res.getString("nis");
                        obj[1] = res.getString("nama");
                        obj[2] = res.getString("nama_kelas");
                        obj[3] = res.getString("jenis_kelamin");
                        obj[4] = res.getString("tahun_ajaran");
                        mDataSiswa.addRow(obj);
                }
                stt.close();
                res.close();
            } catch (SQLException e){
                e.printStackTrace();
                System.out.println(e.getSQLState());
            }
        }
    }

    @Override
    public void filterJurusan(FrameDataNilaiSiswa frm) {
        if(frm.cbb_jurusan.getSelectedItem() == jurusan[1]) {//1 TKJ
            frm.txt_filterJurusan.setText(jurusan[1]);
            getModelTabelSiswa(frm);
            try {    
                mDataSiswa.getDataVector().removeAllElements();
                mDataSiswa.fireTableDataChanged();

                String query = "SELECT * FROM datasiswatkj";
                Statement stt = conn.createStatement();
                ResultSet res = stt.executeQuery(query);
                while(res.next()) {
                    Object[] obj = new Object[5];
                        obj[0] = res.getString("nis");
                        obj[1] = res.getString("nama");
                        obj[2] = res.getString("nama_kelas");
                        obj[3] = res.getString("jenis_kelamin");
                        obj[4] = res.getString("tahun_ajaran");
                        mDataSiswa.addRow(obj);
                }
                stt.close();
                res.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println(e.getSQLState());
            }
        } else if(frm.cbb_jurusan.getSelectedItem() == jurusan[2]) {//2 MM
            frm.txt_filterJurusan.setText(jurusan[2]);
            getModelTabelSiswa(frm);
            try {    
                mDataSiswa.getDataVector().removeAllElements();
                mDataSiswa.fireTableDataChanged();

                String query = "SELECT * FROM datasiswamm";
                Statement stt = conn.createStatement();
                ResultSet res = stt.executeQuery(query);
                while(res.next()) {
                    Object[] obj = new Object[5];
                        obj[0] = res.getString("nis");
                        obj[1] = res.getString("nama");
                        obj[2] = res.getString("nama_kelas");
                        obj[3] = res.getString("jenis_kelamin");
                        obj[4] = res.getString("tahun_ajaran");
                        mDataSiswa.addRow(obj);
                }
                stt.close();
                res.close();
            } catch (SQLException e){
                e.printStackTrace();
                System.out.println(e.getSQLState());
            }
        } else if(frm.cbb_jurusan.getSelectedItem() == jurusan[3]) {//3 elektro
            frm.txt_filterJurusan.setText(jurusan[3]);
            getModelTabelSiswa(frm);
            try {    
                mDataSiswa.getDataVector().removeAllElements();
                mDataSiswa.fireTableDataChanged();

                String query = "SELECT * FROM datasiswaelektro";
                Statement stt = conn.createStatement();
                ResultSet res = stt.executeQuery(query);
                while(res.next()) {
                    Object[] obj = new Object[5];
                        obj[0] = res.getString("nis");
                        obj[1] = res.getString("nama");
                        obj[2] = res.getString("nama_kelas");
                        obj[3] = res.getString("jenis_kelamin");
                        obj[4] = res.getString("tahun_ajaran");
                        mDataSiswa.addRow(obj);
                }
                stt.close();
                res.close();
            } catch (SQLException e){
                e.printStackTrace();
                System.out.println(e.getSQLState());
            }
        } else if(frm.cbb_jurusan.getSelectedItem() == jurusan[4]) {//4 otomotif
            frm.txt_filterJurusan.setText(jurusan[4]);
            getModelTabelSiswa(frm);
            try {    
                mDataSiswa.getDataVector().removeAllElements();
                mDataSiswa.fireTableDataChanged();

                String query = "SELECT * FROM datasiswaotomotif";
                Statement stt = conn.createStatement();
                ResultSet res = stt.executeQuery(query);
                while(res.next()) {
                    Object[] obj = new Object[5];
                        obj[0] = res.getString("nis");
                        obj[1] = res.getString("nama");
                        obj[2] = res.getString("nama_kelas");
                        obj[3] = res.getString("jenis_kelamin");
                        obj[4] = res.getString("tahun_ajaran");
                        mDataSiswa.addRow(obj);
                }
                stt.close();
                res.close();
            } catch (SQLException e){
                e.printStackTrace();
                System.out.println(e.getSQLState());
            }
        }
    }

    @Override
    public void tampilDataSiswa(FrameDataNilaiSiswa frm) throws SQLException {
        getModelTabelSiswa(frm);
        mDataSiswa.getDataVector().removeAllElements();
        mDataSiswa.fireTableDataChanged();
        
        String query = "SELECT nis,nama,nama_kelas,jenis_kelamin,tahun_ajaran FROM datasiswasemua;";
        Statement stt = conn.createStatement();
        ResultSet res = stt.executeQuery(query);
        while(res.next()) {
            Object[] obj = new Object[5];
                obj[0] = res.getString("nis");
                obj[1] = res.getString("nama");
                obj[2] = res.getString("nama_kelas");
                obj[3] = res.getString("jenis_kelamin");
                obj[4] = res.getString("tahun_ajaran");
                mDataSiswa.addRow(obj);
        }
        stt.close();
        res.close();
    }

    @Override
    public void tampiDataNilai(FrameDataNilaiSiswa frm) throws SQLException {
        getModelTabelNilai(frm);
        mDataNilai.getDataVector().removeAllElements();
        mDataNilai.fireTableDataChanged();
        
        String query = "SELECT * FROM tampilseluruhnilai;";
        Statement stt = conn.createStatement();
        ResultSet res = stt.executeQuery(query);
        while(res.next()) {
            Object[] obj = new Object[10];
                obj[0] = res.getString("nis");
                obj[1] = res.getString("nama");
                obj[2] = res.getString("nama_kelas");
                obj[3] = res.getString("nama_pel");
                obj[4] = res.getString("absen");
                obj[5] = res.getString("tugas");
                obj[6] = res.getString("uts");
                obj[7] = res.getString("uas");
                obj[8] = res.getString("total_nilai");
                obj[9] = res.getString("index_nilai");
                mDataNilai.addRow(obj);
        }
        stt.close();
        res.close();
    }

    @Override
    public void tabelSiswaGetClicked(FrameDataNilaiSiswa frm) throws SQLException {
        int row = frm.tbl_dataSiswa.getSelectedRow();
        if(row == -1){
            return;
        }
        frm.txt_nis.setText((String) frm.tbl_dataSiswa.getValueAt(row, 0));
        frm.txt_nama.setText((String) frm.tbl_dataSiswa.getValueAt(row, 1));
        frm.txt_kelas.setText((String) frm.tbl_dataSiswa.getValueAt(row, 2));
        frm.txt_jenisKelamin.setText((String) frm.tbl_dataSiswa.getValueAt(row, 3));
        frm.txt_angkatan.setText((String) frm.tbl_dataSiswa.getValueAt(row, 4));
        frm.txt_nisNilai.setText((String) frm.tbl_dataSiswa.getValueAt(row, 0));
    }

    @Override
    public void getPilihPelajaran(FrameDataNilaiSiswa frm) throws SQLException {
        frm.cbb_mapel.removeAllItems();
        Statement stt = conn.createStatement();
        ResultSet res = stt.executeQuery("SELECT nama_pel FROM t_mapel;");
        frm.cbb_mapel.addItem("Pilih Pelajaran");
        while(res.next()) {
            frm.cbb_mapel.addItem(res.getString("nama_pel"));
        }
        stt.close();
        res.close();
    }
    
    public void getNilaiAbsen(FrameDataNilaiSiswa frm) throws SQLException { //testing
        int total = 0;
        frm.txt_absen.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                double absen = Integer.parseInt(frm.txt_absen.getText());
                double tugas = Integer.parseInt(frm.txt_tugas.getText());
                double totalNilai = (absen*0.1+tugas*0.25);
                frm.txt_total.setText(String.valueOf(totalNilai));
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                
            }
        });
    }
    
    private double hitungNilai(double absen, double tugas, double uts, double uas) {
        double totalNilai = 0;
        absen = 0;
        tugas = 0;
        uts = 0;
        uas = 0;
        
        totalNilai = (absen*0.1+tugas*0.20+uts*0.35+uas*0.35);
        
        return totalNilai;
    }
    
    private int getTotal(FrameDataNilaiSiswa frm) { //test1
            int total = 0;

            int absen = Integer.valueOf(frm.txt_absen.getText());
            int tugas = Integer.valueOf(frm.txt_tugas.getText());
            int uts = Integer.valueOf(frm.txt_uts.getText());
            int uas = Integer.valueOf(frm.txt_uas.getText());
            total = (int) (absen*0.1+tugas*0.2+uts*0.35+uas*0.35);
            
            frm.txt_total.setText(String.valueOf(total));
        return total;
    }
    
    public char indexNilai(FrameDataNilaiSiswa frm) {
        char index = '-';
        int total = Integer.valueOf(frm.txt_total.getText());
            if(total >= 45) {
                index = 'C';
                frm.txt_index.setText(String.valueOf(index));
            } else if (total >= 70) {
                index = 'B';
                frm.txt_index.setText(String.valueOf(index));
            } else if (total >= 85) {
                index = 'A';
                frm.txt_index.setText(String.valueOf(index));
            } else if (total <45 && total >0) {
                index = 'D';
                frm.txt_index.setText(String.valueOf(index));
            } else {
                index = '-';
                frm.txt_index.setText(String.valueOf(index));
            }
        return index;
    }
}
